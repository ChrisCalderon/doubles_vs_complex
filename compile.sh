#!/bin/bash

case `uname` in
    Linux)
	CC=gcc
	LDFLAGS="-lm"
	;;
    Darwin)
	CC=clang
	LDFLAGS=""
	;;
    *)
	echo "Unsupported platform."
	exit 1
	;;
esac

CFLAGS="-O2 -std=c99 -Wall -Werror -pedantic $1"

compile() {
    case $2 in
	doubles)
	    $CC $CFLAGS -DPROGNAME=\"$1\" -o $1 main.c $LDFLAGS
	    ;;
	complex)
	    $CC $CFLAGS -DPROGNAME=\"$1\" -DUSE_COMPLEX -o $1 main.c $LDFLAGS
	    ;;
	*)
	    echo "Unsupported variant."
	    exit 1
	    ;;
    esac
}

compile mandelbrot1 doubles
compile mandelbrot2 complex
