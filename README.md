Run `compile.sh` to compile the code into two different binaries, one named mandelbrot1 that uses doubles and one named mandelbrot2 that uses complex. The compile script accepts an argument for add to the compile flags, useful for specifying the `-mtune` or `-mcpu` flag.

Run `benchmark.sh` to benchmark the two binaries. Here is the output on my 2019 Mac Pro, compiled with `./compile.sh -mtune=cascadelake`:

```
Benchmarking doubles...
Benchmarking complex...
Average runtime using doubles: 0.005
Average runtime using complex: 0.015
```

Here is the output on my Late 2005 PowerMac G5, compiled with `./compile.sh -mcpu=G5`:

```
Benchmarking doubles...
Benchmarking complex...
Average runtime using doubles: 0.011
Average runtime using complex: 0.066
```
