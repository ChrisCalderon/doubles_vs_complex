#!/bin/bash
declare -i trials=1000
declare -i innertrials=1000000
declare -i i
declare -i start
declare -i end


echo "Benchmarking doubles..."
start=$SECONDS
for (( i=0; i < trials; i++ ))
do
    ./mandelbrot1 $innertrials
done
end=$SECONDS
DOUBLES_AVG=`python -c "print(( $end - $start ) / float( $trials ))"`

echo "Benchmarking complex..."
start=$SECONDS
for (( i=0; i < trials; i++ ))
do
    ./mandelbrot2 $innertrials
done
end=$SECONDS
COMPLEX_AVG=`python -c "print(( $end - $start ) / float( $trials ))"`


echo "Average runtime using doubles: $DOUBLES_AVG"
echo "Average runtime using complex: $COMPLEX_AVG"
