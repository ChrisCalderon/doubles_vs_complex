#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define RADIUS 2
#define BASE 10


#ifdef USE_COMPLEX
#include <complex.h>
int
mandelbrot(double complex z0, unsigned long trials)
{
  double complex z = 0.0 + 0.0*I;
  int j;
  for(j=0; j<trials; j++){
    z = z*z + z0;
  }
  return cabs(z) > RADIUS;
}
#else
int
mandelbrot(double r0, double i0, unsigned long trials)
{
  double rtemp, r=0.0, i=0.0;
  int j;
  for(j=0; j<trials; j++){
    rtemp = r;
    r = r*r - i*i + r0;
    i = 2*rtemp*i + i0;
  }
  return (r*r + i*i) > (RADIUS * RADIUS);
}
#endif

int
usage()
{
  printf("Usage: ./%s N\n", PROGNAME);
  puts("");
  puts("N must be a positive int and represents the number of iterations to run.");
  return 0;
}


int
main(int argc, char **argv)
{
  const char *help = "-h";
  const char *help_long = "--help";

  if(argc != 2){
    puts("Incorrect argument count. Use --help to show usage information.");
    return 1;
  }

  if(strcmp(argv[1], help) == 0)
    return usage();

  if(strcmp(argv[1], help_long) == 0)
    return usage();

  unsigned long trials = strtoul(argv[1], NULL, BASE);
  if(trials == 0 && strcmp(argv[1], "0") != 0){
    puts("Unable to parse argument. Use --help to show usage information.");
    return 1;
  }

#ifdef USE_COMPLEX
  return mandelbrot((3.0/4.0) + 0.001*I, trials);
#else
  return mandelbrot(3.0/4.0, 0.001, trials);
#endif
}
